:revealjs_theme: solarized
:icons: font

== Software Crafts&middot;wo&middot;manship Rennes

24 février 2022

S05E05

@swcraftrennes - #swcraftsmanship

Slack : http://bit.ly/swcrafts-rennes

== Remerciements

image::img/sponsors.png[width=512px]

image::img/placemeet-logo.svg[]

== icon:calendar[] 10 Mars - Zenika

Sylvain Révéreault / Antoine Cailly / Kevin Danezis

image:img/sylvain-revereault.JPG[width=250px]
image:img/antoine-cailly.png[width=250px]
image:img/kevin-danezis.jpg[width=250px]

*À la découverte d'Accelerate*

== icon:calendar[] Mai

image::img/Pascal-Le-Merrer.jpg[]

*eXtreme Programming : LA méthode Agile ?*

== icon:calendar[] Vendredi 24 juin

image:img/qr-code-billetterie-socrates.png[Flower,400,400]

https://socrates-rennes.github.io/

// QR code généré ici : https://www.qrcode-monkey.com/fr/

== icon:calendar[] Jeudi 24 février

image::img/David-Blanchet.jpg[]

*I � Unicode !*
